package project

import org.springframework.dao.DataIntegrityViolationException

class MemberController {
    def form(){

    }

    def save(){
        def person = new Member(params)
        //person.save()
        //redirect(controller: "index",action: "index")
        try {
            person.save()
            flash.message = "Create User Success"
            redirect(controller: "index", action: "index")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = "Create User Failed"
            redirect(controller: "index", action: "index")
        }
    }

    def edit(Long id) {
        def memberInstance = Member.get(id)
        if (!memberInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'member.label', default: 'Member'), id])
            redirect(action: "list")
            return
        }
        [memberInstance: memberInstance]
    }

    def delete(Long id) {
        def memberInstance = Member.get(id)
        if (!memberInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'member.label', default: 'Member'), id])
            redirect(controller: "index", action: "index")
            return
        }

        try {
            //memberInstance.delete(flush: true)
            memberInstance.delete()
            flash.message = "Member ID " + memberInstance.id + " Deleted"
            redirect(controller: "index", action: "index")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'member.label', default: 'Member'), id])
            redirect(controller: "index", action: "index")
        }
    }

    def update(Long id, Long version) {
        def memberInstance = Member.get(id)
        if (!memberInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'member.label', default: 'Member'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (memberInstance.version > version) {
                memberInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'member.label', default: 'Member')] as Object[],
                        "Another user has updated this Member while you were editing")
                render(view: "edit", model: [memberInstance: memberInstance])
                return
            }
        }
        memberInstance.properties = params

        if (!memberInstance.save(flush: true)) {
            render(view: "edit", model: [memberInstance: memberInstance])
            return
        }

        flash.message = "Edit User Success"
        redirect(controller: "index", action: "index")
    }

    def detail(Long id) {
        def memberInstance = Member.get(id)
        if (!memberInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'member.label', default: 'Member'), id])
            redirect(action: "list")
            return
        }
        [memberInstance: memberInstance]
    }

    def cancel() {
        redirect(controller: "index", action: "index")
    }
}
