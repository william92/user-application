package project

import grails.converters.*

class MemberAPIController {
    def show(){
        if (params.id && Member.exists(params.id)) {
            def member = Member.findById(params.id)
            render member as JSON
        }
        else{
            def all = Member.list()
            render all as JSON
        }
    }

    def update(){
        def member = Member.findById(params.id)
        member.properties = params
    }

    def delete(){
        def member = Member.findById(params.id)
        member.delete()
    }

    def save(){
        def member = new Member(params)
        member.save()
    }
}
