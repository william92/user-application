package project


class IndexController {

    def index() {
        [contohMember : Member.list()]
    }

    def create() {
        redirect(controller:"member", action:"form")
    }
}
