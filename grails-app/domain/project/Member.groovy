package project

import com.nanlabs.grails.plugin.logicaldelete.LogicalDelete

@LogicalDelete
class Member {
    String nama
    String hp
    String email

    static hasMany = [alamat : String]

    static constraints = {
        nama blank : false, maxSize: 50
        hp blank : false, minSize: 10
        email blank : false, email : true
        alamat blank : false, maxSize: 100
    }
}