class UrlMappings {

	static mappings = {
		"/$controller/$action?/$id?"{
			constraints {
				// apply constraints here
			}
		}
		"/api/member/$id?"(controller: "memberAPI", parseRequest: true) {
			action = [GET: "show", PUT: "update", DELETE: "delete", POST: "save"]
		}

		"/"(controller: 'index', action: 'index')
		"500"(view:'/error')
	}
}
