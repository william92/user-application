<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>User List</title>
		<script type="text/javascript" src="${resource(dir: 'js', file: 'jquery.dataTables.min.js')}"></script>
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'jquery.dataTables.min.css')}"/>
		<style>
			#flash_message{
				position: absolute;
				width: 500px;
			}
		</style>
	</head>
	<body>
	<script type="text/javascript">
		$(document).ready(function() {
			setTimeout(function() {
				$('#flash_message').fadeOut('slow');
			}, 1800);

			$('#tabel_user').DataTable({
				bInfo:false,
				paginate:false,
				"columns": [
					{"name": "first", "orderable": true},
					{"name": "second", "orderable": true},
					{"name": "third", "orderable": false},
					{"name": "fourth", "orderable": false}
				]
			});
			$('#search_input').keyup(function(){
				$('#tabel_user').column(0).search($(this).val()).draw();
			});
		});
	</script>
			<div class="form-horizontal">
				<g:if test="${flash.message}">
					<g:if test="${flash.message=="Create User Success"||flash.message=="Edit User Success"}">
						<div id="flash_message" class="alert alert-success">
							<strong>${flash.message}</strong>
						</div>
					</g:if>
					<g:else>
						<div id="flash_message" class="alert alert-danger">
							<strong>${flash.message}</strong>
						</div>
					</g:else>
				</g:if>
				<g:if test="${contohMember}">
				<table id="tabel_user" class="table table-striped">
					<thead>
					<tr>
						<td>Name</td>
						<td>Email</td>
						<td>Phone</td>
						<td>Action</td>
					</tr>
					</thead>
					<tbody>
					<g:each in="${contohMember}" var="member">
						<tr>
						<td>${member.nama}</td>
						<td>${member.email}</td>
							<td>${member.hp}</td>
							<td>
								<g:link controller="member" action="edit" id="${member.id}"><button class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil"></span> Edit</button></g:link>
								<g:link controller="member" action="detail" id="${member.id}"><button class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-book"></span> Detail</button></g:link>
								<g:link controller="member" action="delete" id="${member.id}"><button class="btn btn-danger btn-xs" onclick="return confirm('Are you sure want to delete ID '+ ${member.id} + '?')"><span class="glyphicon glyphicon-remove-sign"></span> Delete</button></g:link>
							</td>
						</tr>
					</g:each>
					</tbody>
				</table><br/>
				</g:if>
				<g:else>
					<h2>No data, please click the create button</h2>
				</g:else>
				<g:link action="create"><button class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Create</button></g:link>
			</div>
	</body>
</html>
