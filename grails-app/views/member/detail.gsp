<%--
  Created by IntelliJ IDEA.
  User: William
  Date: 08/09/2016
  Time: 13:42
--%>

<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Detail User</title>
</head>
<body>
<br/>
<div class="form-horizontal">
<table id="tabel_detail" class="table table-hover">
    <tr>
        <td><b>Name:</b></td>
        <td>${memberInstance.nama}</td>
    </tr>
    <tr>
        <td><b>Phone:</b></td>
        <td>${memberInstance.hp}</td>
    </tr>
    <tr>
        <td><b>Email:</b></td>
        <td>${memberInstance.email}</td>
    </tr>
    <g:each in="${memberInstance.alamat}" var="member" status="i">
        <tr>
            <g:if test="${memberInstance.alamat.size()>1}">
                <td><b>Alamat #${i+1}:</b></td>
            </g:if>
            <g:else>
                <td><b>Alamat:</b></td>
            </g:else>
            <td>${member}</td>
        </tr>
    </g:each>
</table>
    <a class="btn btn-danger" href="${createLink(uri:'/')}"><span class="glyphicon glyphicon-arrow-left"></span> Back</a>
    <g:link controller="member" action="edit" id="${memberInstance.id}"><button class="btn btn-primary"><span class="glyphicon glyphicon-pencil"></span> Edit</button></g:link>
</div>
</body>
</html>