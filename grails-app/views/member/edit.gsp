<%--
  Created by IntelliJ IDEA.
  User: William
  Date: 08/09/2016
  Time: 9:35
--%>

<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Form Edit User</title>
    <script type="text/javascript">
        $(document).ready(function(){

            var counter = ${memberInstance.alamat.size()}+1;

            $("#form_user").on("click","#addButton", function () {
                if(counter>10){
                    alert("Only 10 textboxes allow");
                    return false;
                }
                $('#button_save').before('<div class="form-group"> <label for="alamat" class="control-label col-xs-2"><span class="glyphicon glyphicon-home"></span> Address #' + counter + ' :</label> <div class="col-xs-6"><g:textField name="alamat" class="form-control" placeholder="input address" required=""/></div><button type="button" id="removeButton" class="btn btn-danger"><span class="glyphicon glyphicon-minus-sign"></span></button></div>')
                counter++;
            });

            $("#form_user").on("click","#removeButton", function(){
                $(this).parent('div').remove();
                counter--;
            });
        });
    </script>
</head>
<body>
<br/>
<g:form name="form_user" class="form-horizontal" controller="member" action="update">
    <g:hiddenField name="id" value="${memberInstance?.id}" />
    <g:hiddenField name="version" value="${memberInstance?.version}" />
    <div class="form-group">
        <label for="nama" class="control-label col-xs-2"><span class="glyphicon glyphicon-user"></span> Name : </label>
        <div class="col-xs-6">
            <g:textField name="nama" value="${memberInstance?.nama}" maxlength="50" class="form-control" placeholder="input name" required=""/>
        </div>
    </div>
    <div class="form-group">
        <label for="hp" class="control-label col-xs-2"><span class="glyphicon glyphicon-phone"></span> Phone : </label>
        <div class="col-xs-6">
            <g:textField name="hp" value="${memberInstance?.hp}" pattern="[0-9].{9,}" class="form-control" placeholder="input phone number" required=""/>
        </div>
    </div>
    <div class="form-group">
        <label for="email" class="control-label col-xs-2"><span class="glyphicon glyphicon-envelope"></span> Email : </label>
        <div class="col-xs-6">
            <g:field type="email" value="${memberInstance?.email}" name="email" class="form-control" placeholder="input email" required=""/>
        </div>
    </div>
    <g:each in="${memberInstance?.alamat}" var="member" status="i">
    <div class="form-group">
        <label for="alamat" class="control-label col-xs-2"><span class="glyphicon glyphicon-home"></span> Address #${(i+1)} : </label>
        <div class="col-xs-6">
            <g:textField name="alamat" value="${member}" class="form-control" placeholder="input address" required=""/>
        </div>
        <g:if test="${i==0}">
            <button type="button" id="addButton" class="btn btn-primary"><span class="glyphicon glyphicon-plus-sign"></span></button>
        </g:if>
        <g:else>
            <button type="button" id="removeButton" class="btn btn-danger"><span class="glyphicon glyphicon-minus-sign"></span></button>
        </g:else>
    </div>
    </g:each>
    <div id="button_save" class="form-group">
        <div class="col-xs-offset-4 col-xs-10">
            <g:actionSubmit class="btn btn-primary" value="Update"/>
            <a class="btn btn-danger" href="${createLink(uri:'/')}">Back</a>
        </div>
    </div>
</g:form>
</body>
</html>