<%--
  Created by IntelliJ IDEA.
  User: William
  Date: 06/09/2016
  Time: 10:05
--%>

<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Form Create User</title>
    <script type="text/javascript">
        $(document).ready(function(){

            var counter = 2;

            $("#addButton").click(function () {
                if(counter>10){
                    alert("Only 10 textboxes allow");
                    return false;
                }
                $('#button_save').before('<div class="form-group"> <label for="alamat" class="control-label col-xs-2"><span class="glyphicon glyphicon-home"></span> Address #' + counter + ' :</label> <div class="col-xs-6"><g:textField name="alamat" class="form-control" placeholder="input address" required=""/></div><button type="button" id="removeButton" class="btn btn-danger"><span class="glyphicon glyphicon-minus-sign"></span></button></div>')
                counter++;
            });

            $("#form_user").on("click","#removeButton", function(e){
                e.preventDefault();
                $(this).parent('div').remove();
                counter--;
            });
        });
    </script>
</head>
<body>
<br/>
<g:form name="form_user" class="form-horizontal" controller="member" action="save">
    <div class="form-group">
        <label for="nama" class="control-label col-xs-2"><span class="glyphicon glyphicon-user"></span> Name : </label>
        <div class="col-xs-6">
            <g:textField name="nama" maxlength="50" class="form-control" placeholder="input name" required=""/>
        </div>
    </div>
    <div class="form-group">
    <label for="hp" class="control-label col-xs-2"><span class="glyphicon glyphicon-phone"></span> Phone : </label>
        <div class="col-xs-6">
            <g:textField name="hp" pattern="[0-9].{9,}" class="form-control" placeholder="input phone number" required=""/>
        </div>
    </div>
    <div class="form-group">
        <label for="email" class="control-label col-xs-2"><span class="glyphicon glyphicon-envelope"></span> Email : </label>
        <div class="col-xs-6">
            <g:field type="email" name="email" class="form-control" placeholder="input email" required=""/>
        </div>
    </div>
    <div class="form-group">
        <label for="alamat" class="control-label col-xs-2"><span class="glyphicon glyphicon-home"></span> Address #1 : </label>
        <div class="col-xs-6">
            <g:textField name="alamat" class="form-control" placeholder="input address" required=""/>
        </div>
        <!--input type='button' value='+' id='addButton'-->
        <button type="button" id="addButton" class="btn btn-primary"><span class="glyphicon glyphicon-plus-sign"></span></button>
    </div>
    <div id="button_save" class="form-group">
        <div class="col-xs-offset-4 col-xs-10">
            <g:actionSubmit class="btn btn-primary" value="Save"/>
            <a class="btn btn-danger" href="${createLink(uri:'/')}">Back</a>
        </div>
    </div>
</g:form>
</body>
</html>